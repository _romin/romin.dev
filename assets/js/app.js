import {
  gsap,
  TimelineMax,
  Power4,
  Elastic,
  TweenMax,
  SplitText,
  ScrollTrigger,
} from 'gsap/all';

import './cursor';
import './smoothScroll';

import WOW from 'wowjs';

const wow = new WOW.WOW(
  {
    live: false,
    boxClass: 'wow',
    animateClass: 'animate__animated',
    mobile: true,
  },
);
wow.init();

gsap.registerPlugin(TimelineMax, ScrollTrigger);

const tl = gsap.timeline();
const transition = new TimelineMax();

/**
 * Sticky Section.
 */
const stickyElement = document.querySelector('.sticky');

if (window.innerWidth >= 768 && stickyElement) {
  gsap.to('.sticky__heading', {
    scrollTrigger: {
      trigger: '.sticky',
      start: 'top center',
      end: 'bottom top-=200',
      scrub: 2,
    },
    duration: 0.25,
    y: stickyElement.clientHeight,
    ease: 'none',
  });
}

/**
 * Split Text Heading.
 */
if (document.querySelector('.hero__heading')) {
  const heroSplitText = new SplitText('.hero__heading', { type: 'words,chars' });
  const { chars } = heroSplitText;

  gsap.set('.hero__heading', { perspective: 400 });
  tl.from(chars, {
    duration: 1,
    delay: 4.5,
    opacity: 0,
    scale: 0,
    y: 80,
    rotationX: 130,
    transformOrigin: '0% 50% -50',
    ease: 'back',
    stagger: 0.01,
  }, '+=0');
}

/**
 * Splash Page Animation.
 */
let splashPageTime = 0;

const colours = [
  'pink', 'purple', 'blue', 'green',
];

colours.forEach((colour) => {
  if (document.querySelector(`.splash__page--${colour}`)) {
    transition.fromTo(`.splash__page--${colour}`, { scaleX: 0 }, {
      transformOrigin: 'left',
      duration: 2,
      scaleX: 1,
      ease: Power4.easeInOut,
    }, splashPageTime);

    splashPageTime += 0.25;
  }
});

colours.forEach((colour, i) => {
  if (document.querySelector(`.splash__page--${colour}`)) {
    if (i === colours.length - 1) {
      transition.to(`.splash__page--${colour}`, {
        scaleX: 0,
        duration: 2.5,
        transformOrigin: 'right',
        ease: Power4.easeInOut,
      });
    }

    transition.set(`.splash__page--${colour}`, { scaleX: 0 });
    transition.set('.splash__page--default', { scaleX: 0 });
  }
});

/**
 * Animate Navigation Links.
 */
if (document.querySelector('.nav__link--project')) {
  TweenMax.staggerFrom('.nav__link--project', 2, {
    scale: 0.5,
    delay: 2.5,
    opacity: 0,
    ease: Elastic.easeOut,
  }, 0.2);
}
if (document.querySelector('.nav__link--home')) {
  TweenMax.staggerFrom('.nav__link--home', 2, {
    scale: 0.5,
    delay: 4.5,
    opacity: 0,
    ease: Elastic.easeOut,
  }, 0.2);
}

function loadProjects() {
  gsap.utils.toArray('.project').forEach((section) => {
    const cover = section.querySelector('.project__cover');
    if (cover) {
      gsap.to(cover, {
        scaleX: 0,
        duration: 1.5,
        transformOrigin: 'right',
        ease: Power4.easeInOut,
        scrollTrigger: section,
      });
    }
  });
}

gsap.delayedCall(5, loadProjects);

/**
 * Scroll To.
 *
 * On click, scroll to paticular section.
 */
const links = document.querySelectorAll('.nav__link--scroll');

function scrollToSection(e) {
  e.preventDefault();
  const href = this.getAttribute('href');
  const { offsetTop } = document.querySelector(href);

  window.scroll({
    top: offsetTop,
    behavior: 'smooth',
  });
}

links.forEach((link) => {
  link.addEventListener('click', scrollToSection);
});

/**
 * Project Overlay.
 *
 * On view, project image sliding in.
 */
if (document.querySelector('.project__overlay')) {
  transition.fromTo('.project__overlay', { scaleX: 0 }, {
    transformOrigin: 'left',
    duration: 1.75,
    scaleX: 1,
    ease: Power4.easeInOut,
  }, 0.25);

  gsap.delayedCall(2, () => {
    const content = document.querySelector('.project__content');
    TweenMax.fromTo(content, 1, { opacity: 0, y: 50 }, { opacity: 1, y: 0 });
  });
}

/**
 * Theme Switcher
 *
 * A button to toggle dark theme class.
 */
const themeSwitcher = document.getElementById('theme-switcher');

if (themeSwitcher) {
  const themeSwitcherIcon = themeSwitcher.children[0];

  themeSwitcher.addEventListener('click', () => {
    const body = document.getElementsByTagName('body')[0];

    body.classList.toggle('themes__dark');
    themeSwitcherIcon.classList.toggle('far');
    themeSwitcherIcon.classList.toggle('fas');
  });
}

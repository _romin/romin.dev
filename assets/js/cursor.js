import {
  TweenLite,
} from 'gsap/all';

const $circle = document.querySelector('.circle');
const $follow = document.querySelector('.circle-follow');

function moveCircle(e) {
  TweenLite.to($circle, 0.3, {
    x: e.clientX,
    y: e.clientY,
  });
  TweenLite.to($follow, 0.7, {
    x: e.clientX,
    y: e.clientY,
  });
}

function hoverFunc() {
  TweenLite.to($circle, 0.3, {
    opacity: 1,
    scale: 0,
  });
  TweenLite.to($follow, 0.3, {
    scale: 3,
  });
}

function unhoverFunc() {
  TweenLite.to($circle, 0.3, {
    opacity: 1,
    scale: 1,
  });
  TweenLite.to($follow, 0.3, {
    scale: 1,
  });
}

window.addEventListener('mousemove', moveCircle);

const links = document.getElementsByTagName('a');

for (let i = 0; i < links.length; i++) {
  links[i].addEventListener('mouseover', hoverFunc);
  links[i].addEventListener('mouseout', unhoverFunc);
}
